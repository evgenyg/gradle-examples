import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
import static ch.qos.logback.classic.Level.*


/**
 * http://logback.qos.ch/manual/groovy.html
 */

appender( 'CONSOLE', ConsoleAppender ) {
    encoder( PatternLayoutEncoder ) {
        pattern = "[%date{ISO8601}][%level][%logger] - [%msg]%n"
    }
}

root( DEBUG, [ 'CONSOLE' ])
