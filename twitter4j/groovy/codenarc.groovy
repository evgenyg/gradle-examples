
ruleset {

    description 'GCommons CodeNarc RuleSet'

    ruleset( "http://codenarc.sourceforge.net/StarterRuleSet-AllRulesByCategory.groovy.txt" ) {

        DuplicateNumberLiteral   ( enabled : false )
        DuplicateStringLiteral   ( enabled : false )
        BracesForClass           ( enabled : false )
        BracesForMethod          ( enabled : false )
        BracesForIfElse          ( enabled : false )
        BracesForForLoop         ( enabled : false )
        BracesForTryCatchFinally ( enabled : false )
        JavaIoPackageAccess      ( enabled : false )
        JUnitPublicNonTestMethod ( enabled : false )

        LineLength               ( length     : 160   )
        VariableName             ( finalRegex : /[a-zA-Z]+/ )
        MethodName               ( regex      : /[a-zA-Z][\w\s'\(\)]*/ ) // Spock method names
    }
}
