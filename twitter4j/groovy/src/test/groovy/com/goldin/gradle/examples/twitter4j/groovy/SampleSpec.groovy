package com.goldin.gradle.examples.twitter4j.groovy

import spock.lang.Specification

class SampleSpec extends Specification
{
    def "expect-where"( int x, int y, int z )
    {
        expect:
        x + y == z

        where:
        x | y | z
        1 | 2 | 3
        2 | 3 | 5
    }


    def "when-then"()
    {
        when:
        def x = 3
        def y = 5

        then:
        x * y == 15
    }


    def "when-then-where"( int z, int w )
    {
        when:
        def x = 3
        def y = 5

        then:
        ( x * y ) + z == w

        where:
        z << [1, 10, 100 ]
        w << [16, 25, 115]
    }


    def "expect-where-2"( int x, int y, int z )
    {
        expect:
        x + y == z

        where:
        x << [1, 2]
        y << [2, 3]
        z << [3, 5]
    }


    def "setup-expect"()
    {
        setup:
        def x = 3

        expect:
        x == 3
    }


    def "setup-when-then"()
    {
        setup:
        def x = 3

        when:
        x++

        then:
        x == 4
    }


    def "given-when-then"()
    {
        given:
        def x = 3

        when:
        x *= 2

        then:
        x == 6
    }
}
