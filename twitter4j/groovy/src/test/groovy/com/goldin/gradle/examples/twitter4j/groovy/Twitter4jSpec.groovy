package com.goldin.gradle.examples.twitter4j.groovy

import spock.lang.Specification
import twitter4j.Query
import twitter4j.QueryResult
import twitter4j.Tweet
import twitter4j.Twitter

/**
 * {@link Twitter4j} Spock specs
 */
class Twitter4jSpec extends Specification
{
    def "Twitter returning single result" ()
    {
        given:
        Twitter     twitter = Mock( Twitter )
        QueryResult result  = Mock( QueryResult )
        Tweet       tweet   = Mock( Tweet )

        Query query         = new Query( '#groovy' )
        query.rpp           = 1

        when:
        def l = new QueryRunner( twitter, [ query.query ], 1, 100 ).run()

        then:
        l.size() == 1
        1 * twitter.search( query ) >> result
        1 * result.tweets           >> [ tweet ]
        1 * tweet.isoLanguageCode   >> 'en'
        0 * tweet.id
    }


    def "Twitter returning multiple results" ()
    {
        given:
        Twitter     twitter = Mock( Twitter )
        QueryResult result  = Mock( QueryResult )
        Tweet       tweet   = Mock( Tweet )

        Query query         = new Query( '#groovy' )
        query.rpp           = 3

        when:
        def l = new QueryRunner( twitter, [ query.query ], 3, 100 ).run()

        then:
        l.size() == 3
        1 * twitter.search( query ) >> result
        1 * result.tweets           >> [ tweet, tweet, tweet ]
        3 * tweet.isoLanguageCode   >> 'en'
        0 * tweet.id
    }


    def "Twitter returning single result multiple times" ()
    {
        given:
        Twitter     twitter = Mock( Twitter )
        QueryResult result  = Mock( QueryResult )
        Tweet       tweet1  = Mock( Tweet )
        Tweet       tweet2  = Mock( Tweet )
        Tweet       tweet3  = Mock( Tweet )
        Query       query   = new Query( '#groovy' )
        query.rpp           = 3

        when:
        def l = new QueryRunner( twitter, [ query.query ], 3, 100 ).run()

        then:
        l.size() == 3
        3 * twitter.search( query )  >>> [ result, result, result ]
        result.tweets                >>> [[ tweet1 ], [ tweet2 ], [ tweet3 ]]
        _.isoLanguageCode            >> 'en'
        tweet1.id                    >> 1
        tweet2.id                    >> 2
        tweet3.id                    >> 3
    }
}
