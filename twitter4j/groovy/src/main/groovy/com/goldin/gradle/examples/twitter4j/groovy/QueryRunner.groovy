package com.goldin.gradle.examples.twitter4j.groovy

import groovy.util.logging.Log
import org.gcontracts.annotations.Ensures
import org.gcontracts.annotations.Requires
import twitter4j.Query
import twitter4j.Tweet
import twitter4j.Twitter

import java.util.logging.Level

@Log
class QueryRunner
{
    private  final Twitter     twitter
    private  final List<Query> queries
    private  final int         nResults
    private  final long        sleepPeriod


    @Requires({ twitter && queries && ( nResults > 0 ) && ( sleepPeriod > 0 ) })
    QueryRunner ( Twitter twitter, List<String> queries, int nResults = 5, long sleepPeriod = 30000 )
    {
        this.twitter     = twitter
        this.queries     = queries.collect { String s -> def q = new Query( s ); q.rpp = [ 100, nResults ].min(); q }
        this.nResults    = nResults
        this.sleepPeriod = sleepPeriod
    }


    @Ensures({ result.size() >= nResults })
    List<TweetWithQuery> run ()
    {
        log.info( "Twitter4j - Groovy: Running quer${ queries.size() == 1 ? 'y' : 'ies' } ${ queries*.query }" )

        Set<TweetWithQuery> tweets = [] as Set

        L:
        while( true )
        {
            for ( Query query in queries )
            {
                try
                {
                    def tweetsFound = twitter.search( query ).tweets
                    tweets.addAll( tweetsFound.
                                   findAll { Tweet t -> ( 'en' == t.isoLanguageCode ) && ( ! tweets.any{ it.id == t.id } )}.
                                   collect { Tweet t -> new TweetWithQuery( t, query ) })
                }
                catch ( e )
                {
                    log.log( Level.SEVERE, "Failed to query Twitter for [$query.query]", e )
                }

                log.info( "[${ tweets.size() }] tweets so far" )

                if ( tweets.size() < nResults ) { sleep( sleepPeriod ) }
                else                            { break L }
            }
        }

        log.info( "[${ tweets.size() }] tweets found" )
        tweets as List
    }


    @Override
    @Ensures({ result })
    String toString ()
    {
        run().collect { TweetWithQuery t ->
                        """
                        <tweet>
                        |    <query>$t.query.query</query>
                        |    <id>$t.id</id>
                        |    <fromUser>$t.fromUser</fromUser>
                        |    <fromUserId>$t.fromUserId</fromUserId>
                        |    <toUser>$t.toUser</toUser>
                        |    <toUserId>$t.toUserId</toUserId>
                        |    <createdAt>$t.createdAt</createdAt>
                        |    <text>$t.text</text>
                        |    <isoLanguageCode>$t.isoLanguageCode</isoLanguageCode>
                        |    <source>$t.source</source>
                        |    <profileImageUrl>$t.profileImageUrl</profileImageUrl>
                        |    <geoLocation>$t.geoLocation</geoLocation>
                        |    <location>$t.location</location>
                        |    <place>$t.place</place>
                        |</tweet>""".stripMargin() }*.trim().join( '\n' ) + '\n'
    }


    @Requires({ file })
    @Ensures({ file.file })
    void write ( File file )
    {
        assert (( ! file.file ) || file.delete())

        file.parentFile.mkdirs()
        file.write( toString(), 'UTF-8' )
    }
}
