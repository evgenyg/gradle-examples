package com.goldin.gradle.examples.twitter4j.groovy

import twitter4j.TwitterFactory
import groovy.util.logging.Log

@Log
class Twitter4j
{
    static void main ( String ... args )
    {
        assert args.size() == 2, 'Arguments expected: <Twitter query> <Result file>'
        def    query    = args[ 0 ]
        def    fileName = args[ 1 ]
        assert query && fileName

        def queries = ( query.startsWith( 'file:' )) ?
            new File( query[ 'file:'.size() .. -1 ] ).readLines()*.trim().grep() :
            [ query ]

        new File( fileName ).with {
            new QueryRunner( new TwitterFactory().instance, queries ).write( delegate )
            log.info( "Tweets written to [$delegate.canonicalPath]" )
        }
    }
}
