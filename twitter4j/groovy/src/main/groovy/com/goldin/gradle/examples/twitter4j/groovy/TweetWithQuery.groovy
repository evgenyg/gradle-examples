package com.goldin.gradle.examples.twitter4j.groovy

import org.gcontracts.annotations.Requires
import twitter4j.Query
import twitter4j.Tweet

/**
 * {@link twitter4j.Tweet} implementation remembering the original query
 */
class TweetWithQuery implements Tweet {

    @Delegate final Tweet tweet
              final Query query


    @Requires({ tweet && query })
    TweetWithQuery ( Tweet tweet, Query query )
    {
        this.tweet = tweet
        this.query = query
    }


    @Override
    @Requires({ t })
    int compareTo ( Tweet t ) { tweet.compareTo( t ) }
}
