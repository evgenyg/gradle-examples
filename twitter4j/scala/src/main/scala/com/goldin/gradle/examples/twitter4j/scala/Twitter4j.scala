package com.goldin.gradle.examples.twitter4j.scala

import org.slf4j.LoggerFactory

object Twitter4j extends App
{
    val logger = LoggerFactory.getLogger( Twitter4j.getClass )

    assert( args.length == 1 )
    val query = args(0)
    assert( query != null )

    logger.info( "Twitter4j - Scala: Running query [" + args.mkString( ", " ) + "]" )
    logger.info( "Twitter4j - Scala:\n" + new QueryRunner( query ).result )
}
