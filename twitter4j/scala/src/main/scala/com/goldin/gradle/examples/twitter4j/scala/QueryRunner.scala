package com.goldin.gradle.examples.twitter4j.scala

import scala.collection.JavaConversions._
import twitter4j.{TwitterFactory , Query}


class QueryRunner( val query:String )
{
    def result:String = {
        new TwitterFactory().getInstance.search( new Query( query )).getTweets.
        map{ t => "[" + t.getFromUser + "]: [" + t.getText + "]" }.
        mkString( "\n" )
    }
}