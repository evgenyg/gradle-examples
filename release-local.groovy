buildscript {
	repositories {
		ivy {
			artifactPattern "${ System.getProperty( 'user.home' ) }/Projects/gradle-release/build/libs/[artifact]-[revision].jar"
		}
	}
	dependencies {
		classpath 'gradle-release:gradle-release:0.8a'
	}
}

project.apply( plugin: release.ReleasePlugin )
